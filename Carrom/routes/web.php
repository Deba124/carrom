<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Users;
use App\Http\Controllers\AdminCheck;
use App\Http\Controllers\Logout;
use App\Http\Controllers\Activities;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('admin', function () {
    return view('Admin/Pages/adminlogin');
});
Route::get('dashboard', function () {
    return view('Admin/Pages/dashboard');
});
Route::get('servicedown', function () {
    return view('Admin/Pages/serviceDown');
});
Route::get('users', function () {
    return view('Admin/Pages/users');
});
Route::get('transaction', function () {
    return view('Admin/Pages/transactions');
});
Route::get('activity', function () {
    return view('Admin/Pages/activityPage');
});
Route::get('match', function () {
    return view('Admin/Pages/matchesView');
});
Route::get('paymentStat', function () {
    return view('Admin/Pages/paymentDetails');
});
Route::get('userTran', function () {
    return view('Admin/Pages/userTransaction');
});

Route::get('/checkdb',[Users::class,'DbCheck']);
Route::post('setPayment',[Users::class,'setPaymentThroughCashfree']);
Route::get('/checkIns',[Users::class,'InitTransaction']);
Route::get('logout',[Logout::class,'getOut']);
Route::post("adminCheck",[AdminCheck::class,'CheckAdmin']);
Route::post("block",[Activities::class,'blockUser']);
Route::get("unblock",[Activities::class,'unblockUser']);
Route::get("activate",[Activities::class,'enableOrDiableToken']);
Route::post("updateSpin",[Activities::class,'updateSpinValues']);
Route::get('promote',[Users::class,'promoteUser']);
Route::get('demote',[Users::class,'demoteUser']);
Route::get('friends', function () {
    return view('Admin/Pages/friends');
});
Route::get('league', function () {
    return view('Admin/Pages/leagues');
});
Route::get('approve',[Users::class,'acceptWithdraw']);
Route::get('decline',[Users::class,'rejectWithdraw']);
Route::get('distribute_daily',[Users::class,'DailyPaisa']);
Route::get('distribute_weekly',[Users::class,'weeklyPaisa']);
