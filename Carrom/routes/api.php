<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Users;
use App\Http\Controllers\Usersrelated;
use App\Http\Controllers\Activities;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login',[Usersrelated::class,'SignInOrSignUp']);
Route::post('register',[Users::class,'SignUp']);
Route::post('update',[Users::class,'updateProfile']);
Route::post('stat',[Users::class,'changeStat']);
Route::post('registerDevId',[Users::class,'uploadDeviceId']);
Route::get('tokens',[Users::class,'getTokens']);
Route::get('spins',[Users::class,'getAvailableSpins']);
Route::post('join',[Users::class,'joinMatch']);
Route::post('create',[Users::class,'createMatch']);
Route::post('canceled',[Users::class,'matchIsCanceled']);
Route::post('updateBalance',[Users::class,'moneyOperations']);
Route::post('setTemp',[Users::class,'setTempPay']);
Route::post('wallet',[Users::class,'getWalletUpdate']);
Route::post('matchDetails',[Users::class,'getMatchDetails']);
Route::post('win',[Users::class,'showWin']);
Route::post('addBenificary',[Users::class,'addBenificiary']);
Route::post('details',[Users::class,'GetGeneralDetails']);
Route::post('bankdetails',[Users::class,'SetBankDetails']);
Route::post('checkbank',[Users::class,'GetBankStatus']);
Route::post('withdraw',[Users::class,'GetWithdrawRequest']);
Route::post('allBalance',[Users::class,'GetAllBalanceDetails']);
Route::post('checkDevice',[Users::class,'checkDevId']);
Route::get('version',[Users::class,'checkVersion']);
Route::get('league',[Users::class,'getLeagues']);
Route::post('leagueJoin',[Users::class,'joinLeague']);
Route::post('leagueCheck',[Users::class,'getLeagueInfo']);
Route::post('participants',[Users::class,'getMyLeaguePariticiapnts']);
Route::post('addcoins',[Users::class,'CoinUpdate']);
Route::post('match',[Users::class,'AddMatchCount']);
Route::post('checkac',[Users::class,'checkBank']);
Route::post('setdetails',[Users::class,'setDetailsBank']);
Route::get('daily',[Users::class,'getDailyRank']);
Route::get('weekly',[Users::class,'getWeeklyRank']);
Route::get('all',[Users::class,'getAllRank']);
Route::post('setDaily',[Users::class,'SetDailyCoin']);
Route::post('paymentRequest',[Users::class,'GetPaymentRequest']);
Route::post('pic',[Users::class,'updateProfPic']);
Route::get('myRank',[Users::class,'GetMyPosition']);
Route::post('pendingRequest',[Users::class,'getPendingRequest']);
