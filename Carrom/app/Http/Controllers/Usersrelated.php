<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
class Usersrelated extends Controller
{
    function SignInOrSignUp(Request $request)
    {
        $user = User::where('devId',$request->devid)->first();

        if($user)
        {
            if(!empty($request->rank)){
            User::where('devId',$request->devid)->update(["coins"=>$request->coins,"rank"=>$request->rank]);
            }
            else{
                User::where('devId',$request->devid)->update(["coins"=>$request->coins]);
            }
            return response([
                
                "response_data"=>$user,
                
                ],200);
        }
        else
        {
            if(!empty($request->rank)){
            User::insert(["userName"=>$request->userName,"coins"=>$request->coins,"matchWon"=>0,"matchPlayed"=>0,"country"=>"India","rank"=>$request->rank,"devId"=>$request->devid]);
            }
            else
            {
                User::insert(["userName"=>$request->userName,"coins"=>$request->coins,"matchWon"=>0,"matchPlayed"=>0,"country"=>"India","devId"=>$request->devid]);
            }
            $user = User::where('devId',$request->devid)->first();
            return response([
                
                "response_date"=>$user
                
                ],200);
        }
    }
}
