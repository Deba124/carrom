<?php

namespace App\Http\Controllers;
session_start();
if(!isset($_SESSION['login']))
{
    header("Location:admin");
}
use App\Models\Blocked;
use App\Models\User;
use App\Models\Token;
use App\Models\Spin;
use App\Models\Matche;
use App\Models\Result;
use Illuminate\Http\Request;

class Activities extends Controller
{
    function blockUser(Request $req)
    {
        $opt = User::where('id',$req->id)->update(['isBlocked'=>1]);
        if($opt)
        {
            Blocked::insert(['id'=>$req->id,'activity'=>$req->activity,'reason'=>$req->reason]);
            header('Location:users');
        }
    }

    function unblockUser(Request $req)
    {
        //die($req->id);
        $opt = User::where('id',$req->id)->update(['isBlocked'=>0]);

        if($opt)
        {
           Blocked::where('id',$req->id)->delete();
           header('Location:users');
        }
    }

    function enableOrDiableToken(Request $request)
    {
       $dat = Token::where('SN',$request->id)->first();

       if($dat->status == 0)
       {
           Token::where('SN',$request->id)->update(['status'=>1]);
           header("Location:activity");
       }
       else
       {
           Token::where('SN',$request->id)->update(['status'=>0]);
           header("Location:activity");
       }
    }

    function updateSpinValues(Request $request)
    {
        $update = Spin::where("SN",1)->update(["zero"=>$request->one,"one"=>$request->two,"two"=>$request->three,"three"=>$request->four,"four"=>$request->five,"five"=>$request->six]);
        if($update)
        header("location:activity");
    }
    
}
