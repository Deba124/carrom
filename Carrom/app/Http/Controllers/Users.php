<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\TransactionHistory;
use App\Models\Token;
use App\Models\Spin;
use App\Models\Matche;
use App\Models\Payment;
use App\Models\Temppayment;
use App\Models\Result;
use App\Models\Vpa;
use App\Models\Bankdetail;
use App\Models\Paymentrequest;
use App\Models\Version;
use App\Models\League;
use App\Models\Leaguejoin;
 use App\Models\Distribution;
//use App\Models\Bankdetail;
use View;
class Users extends Controller
{
    function CheckHeader(Request $request)
    {
        if($request->header('app-auth')=='gg_ludo_8080')
        {
            return true;
        }

        return false;
    }

    function GetPaymentRequest(Request $request)
    {
        $usr = User::where('devId',$request->userName)->first();
        if($usr->prize > 0){
        Paymentrequest::insert(['userName'=>$request->userName]);
        }
        
        return response("request sent",200);
    }

function CheckVersion()
{
    $version = Version::where('id','1')->first();
    return response($version->currentVersion,200);
}

function GetMyPosition(Request $request)
{
    $daily = User::select('*')->orderBy('daily','desc')->get();
    $weekLy = User::select('*')->orderBy('weekly','desc')->get();
    $all = User::select('*')->get();
    $userData = [];

    $dailyCount = 0;$weeklyCount=0;$allCount=0;

    foreach($daily as $d)
    {
        $dailyCount++;
        if($d->devId == $request->userName)
        {
            
            break;

        }
    }
    $userData["daily"] = $dailyCount;
    foreach($weekLy as $w)
    {
        $weeklyCount++;
        if($w->devId == $request->userName)
        {
            
            break;

        }
    }
    $userData["weekly"]=$weeklyCount;

    foreach($all as $a)
    {
        $allCount++;
        if($a->userName == $request->userName)
        {
            
            break;

        }
    }
    $userData["all"]=$allCount;
    $us = User::where('devId',$request->userName)->first();
    $userData["user"] = $us;
    return response($userData,200);
}

    function SetDailyCoin(Request $request)
    {
        $u = User::where('devId',$request->userName)->first();
        $coinstoBeAdded = 0;
        if($request->amount > 5000)
        {
            $coinstoBeAdded = 5000;
        }
        else
        {
           $coinstoBeAdded = $request->amount;
        }

        if($u)
        {
            User::where('devId',$request->userName)->update(['coins'=>($u->coins+$coinstoBeAdded),'daily'=>($u->daily + $coinstoBeAdded),'weekly'=>($u->weekly + $coinstoBeAdded)]);
            return response("updated daily",200);
        }
    }

    function InitTransaction($amt,$id,$type="Wallet Addition")
    {
         $req = new Request(["amount"=>$amt,"id"=>$id,"type"=>$type]);
         //$req->amount = $amt;
         //$req->id = $id;
         //$req->type="WA";
         $tran = new TransactionHistory();

        return $tran->setTransaction($req);
    }
    function checkBank(Request $request)
    {
        $dat = Bankdetail::where('userName',$request->userName)->first();
        if($dat)
        {
            return response("yes",200);
        }
        else
        {
            return response("no",200);
        }
    }

    function acceptWithdraw(Request $request)
    {
        $dat = Paymentrequest::where('userName',$request->id)->update(['status'=>1]);
        User::where('devId',$request->id)->update(['prize'=>0]);
        header('location:league');
        
    }
    function rejectWithdraw(Request $request)
    {
        $dat = Paymentrequest::where('userName',$request->id)->update(['status'=>2]);
        header('location:league');
        
    }

    function setDetailsBank(Request $request)
    {
        $check = Bankdetail::where('userName',$request->userName)->first();
        if($check)
        {
           $dat = Bankdetail::where('userName',$request->userName)->update(['name'=>$request->name,'vpa'=>$request->vpa]);
        }
        else{
            $dat = Bankdetail::insert(['userName'=>$request->userName,'name'=>$request->name,'vpa'=>$request->vpa]);
        }
        
        if($dat)
        {
            return response("yes",200);
        }
        else
        {
            return response("no",200);
        }
    }

    function AddMatchCount(Request $request)
    {
        $dat = User::where('devId',$request->userName)->first();
        User::where('devId',$request->userName)->update(['matchPlayed'=>($dat->matchPlayed + 1)]);
        return response("updated match count",200);
    }
    function CoinUpdate(Request $request)
    {
        
        User::where('devId',$request->userName)->update(['coins'=>$request->coins]);
        return response("updated coins",200);
    }

    function getLeagues()
    {
        $data = League::select('*')->get();
        return response(["response_data"=>$data],200);
    }

    function joinLeague(Request $request)
    {
         Leaguejoin::insert(["userId"=>$request->userId,"leagueId"=>$request->leagueId]);
         $ins = Leaguejoin::select('*')->orderBy('id','DESC')->first();
        return response($ins,200);
    }

    function getLeagueInfo(Request $request)
    {
        $data = Leaguejoin::where('userId',$request->userId)->first();
        if($data){
        $dat = League::where('id',$data->leagueId)->first();
        return response($dat,200);
        }
        else
        {
            return response("",200);
        }
    }

    function DailyPaisa()
    {
            $amt =[];
            $amt[0] = 60;
            $amt[1] = 50;
            $amt[2] = 40;
            $amt[3] = 30;
            $amt[4] = 20;
            $amt[5] = 15;
            $amt[6] = 15;
            $amt[7] = 15;
            $amt[8] = 15;
            $amt[9] = 15;
            $amt[10] = 10;
            $amt[11] = 10;
            $amt[12] = 10;
            $amt[13] = 10;
            $amt[14] = 10;
            $amt[15] = 10;
            $amt[16] = 10;
            $amt[17] = 10;
            $amt[18] = 10;
            $amt[19] = 10;
            $amt[20] = 10;
            $amt[21] = 5;
            $amt[22] = 5;
            $amt[23] = 5;
            $amt[24] = 5;
            $amt[25] = 5;
            $amt[26] = 5;
            $amt[27] = 5;
            $amt[28] = 5;
            $amt[29] = 5;
            $amt[30] = 5;
            $count = 0;
            $all = User::select('*')->orderBy('daily','desc')->limit(30)->get();
            foreach($all as $usr)
            {
                User::where('userName',$usr->userName)->update(['prize'=>($usr->prize+$amt[$count]),'daily'=>0]);
                $count++;
            }
            $date = date('Y-m-d H:i:s');
            Distribution::where('id',1)->update(['daily'=>$date]);
            $usersAll = User::where('daily','>',0)->get();
            foreach($usersAll as $usrAll)
            {
                User::where('userName',$usrAll->userName)->update(['daily'=>0]);
            }
            header('location:league');
    }

    function weeklyPaisa()
    {
            $amt =[];
            $amt[0] = 100;
            $amt[1] = 90;
            $amt[2] = 80;
            $amt[3] = 70;
            $amt[4] = 50;
            $amt[5] = 20;
            $amt[6] = 20;
            $amt[7] = 20;
            $amt[8] = 20;
            $amt[9] = 20;
            $count = 0;
            $all = User::select('*')->orderBy('weekly','desc')->limit(10)->get();
            foreach($all as $usr)
            {
                User::where('userName',$usr->userName)->update(['prize'=>($usr->prize+$amt[$count]),'weekly'=>0]);
                $count++;
            }

            $date = date('Y-m-d H:i:s');
            Distribution::where('id',1)->update(['weekly'=>$date]);
            $usersAll = User::select('*')->get();
            foreach($usersAll as $usrAll)
            {
                User::where('userName',$usrAll->userName)->update(['weekly'=>0]);
            }
            header('location:league');
    }

    function getMyLeaguePariticiapnts(Request $request)
    {
        $data = Leaguejoin::where('userId',$request->userId)->first();
        if($data)
        {
            $participants = Leaguejoin::where('leagueId',$data->leagueId)->get();
            $part = [];
            $count = 0;
            foreach($participants as $p)
            {
             $user =  User::where('userName',$p->userId)->first();
             $part[$count] = $user;
             $count++;
            }

            return response(["response_data"=>$part],200);
        }
    }
    function getDailyRank()
    {
        $user = User::select('*')->orderBy('daily','desc')->limit(100)->get();
        $dists = Distribution::select('*')->first();
        
        $dailyTime = strtotime( $dists->daily);
        $weeklytime = date("Y-m-d H:i:s", strtotime( $dists->weekly. "+ 7 days"));
        $nowTimeDaily = strtotime(date("Y/m/d 23:59:00"));
        
        $dailyTime = ($nowTimeDaily - strtotime(date("Y/m/d H:i:s")))/60;
        //echo($dailyTime);
        //echo(date(date("Y/m/d 23:59:00")));
        //exit(0);
        $dailyHours = ($dailyTime)/60;
        $hoursOnlyDaily = intval($dailyHours);
        
        $minutesDaily = floor($dailyTime - ($hoursOnlyDaily * 60));
        $weeklytime = (strtotime($weeklytime)-strtotime(date("Y-m-d H:i:s")))/60;
        $d = floor ($weeklytime / 1440);
        $h = floor (($weeklytime - $d * 1440) / 60);
        $m = $weeklytime - ($d * 1440) - ($h * 60);
        $totalWeekTime = $d . "d ".$h. "h ".floor($m)."m";
        return response(["response_data"=>$user,
            "dailyTime"=>$hoursOnlyDaily . "h ".$minutesDaily."m",
            "weeklyTime"=>$totalWeekTime],200);
    }
    function getWeeklyRank()
    {
        $user = User::select('*')->orderBy('weekly','desc')->limit(100)->get();
        $dists = Distribution::select('*')->first();

        $dailyTime = strtotime( $dists->daily);
        $weeklytime = date("Y-m-d H:i:s", strtotime( $dists->weekly. "+ 7 days"));
        $nowTimeDaily = strtotime(date("Y/m/d 23:59:00"));
        
        $dailyTime = ($nowTimeDaily - strtotime(date("Y/m/d H:i:s")))/60;
        //echo($dailyTime);
        //echo(date(date("Y/m/d 23:59:00")));
        //exit(0);
        $dailyHours = ($dailyTime)/60;
        $hoursOnlyDaily = intval($dailyHours);
        
        $minutesDaily = floor($dailyTime - ($hoursOnlyDaily * 60));
        $weeklytime = (strtotime($weeklytime)-strtotime(date("Y-m-d H:i:s")))/60;
        $d = floor ($weeklytime / 1440);
        $h = floor (($weeklytime - $d * 1440) / 60);
        $m = $weeklytime - ($d * 1440) - ($h * 60);
        $totalWeekTime = $d . "d ".$h. "h ".floor($m)."m";
        return response(["response_data"=>$user,
            "dailyTime"=>$hoursOnlyDaily . "h ".$minutesDaily."m",
            "weeklyTime"=>$totalWeekTime],200);
       // return response(["response_data"=>$user],200);
    }
    function getAllRank()
    {
        $user = User::select('*')->orderBy('coins','desc')->limit(100)->get();

        return response(["response_data"=>$user],200);
    }

    function getPendingRequest(Request $request)
    {
       $pays = Paymentrequest::where("userName",$request->id)->where("status",0)->first();

       if($pays)
       {
           return response("pending",200);
       }
       else
       {
           return response("approved",200);
       }
    }

    function updateProfPic(Request $request)
    {
        User::where('userName',$request->userName)->update(["pic"=>$request->pic]);
        return response("updated",200);
    }

    function setPaymentThroughCashfree(Request $request)
    {
        //csrf_field();
        //die($request);
        $tempPay = Temppayment::where('orderId',$request->orderId)->first();
        if($tempPay){
        $getId = Payment::where('orderId',$request->orderId)->first();
        if($getId)
        {
            header('location:paymentStat?type=repeat');
        }
        else
        {
            $url = 'https://test.cashfree.com/api/v1/order/info/status';
// Collection object
$data = [
  'appId' => '88158e67f7fb775e6ff36b3c485188',
  'secretKey'=>'d4255cb85af13f4654909f325eadc93912a97754',
  'orderId'=>$request->oid

];
// Initializes a new cURL session
$curl = curl_init($url);
// Set the CURLOPT_RETURNTRANSFER option to true
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
// Set the CURLOPT_POST option to true for POST request
curl_setopt($curl, CURLOPT_POST, true);
// Set the request data as JSON using json_encode function
curl_setopt($curl, CURLOPT_POSTFIELDS,  $data);
// Set custom headers for RapidAPI Auth and Content-Type header
curl_setopt($curl, CURLOPT_HTTPHEADER, [
  'Content-Type: application/x-www-form-urlencoded'
]);
// Execute cURL request with all previous settings
$response = curl_exec($curl);

$parsedData = json_decode($response);

// Close cURL session
curl_close($curl);
//die(print_r($parsedData));

if($request->txStatus == "SUCCESS")
{
    $user=User::where('id',$tempPay->userId)->first();
    $ins = Payment::insert(["userName"=>$user->userName,"type"=>"added","status"=>"success","amount"=>$request->orderAmount,"orderId"=>$request->orderId]);

    $nowAmt = $user->bounty + $request->orderAmount;
    $update = User::where('id',$tempPay->userId)->update(["bounty"=>$nowAmt]);
    if($update)
    header('location:paymentStat?type=paid');
    else
        header('location:paymentStat?type=declined');
}
else
{
    header('location:paymentStat?type=declined');
}

        }
        }
    }

    function setTempPay(Request $request)
    {
        $ins = Temppayment::insert(["userId"=>$request->userId,"orderId"=>$request->orderId,"amount"=>$request->amount]);
        if($ins)
        {
            return response([
                "response_code"=>"200",
                "response_data"=>"success",
                "response_msg"=>"temp payment is made"
                ],200);
        }
    }

    //function checkVersion()
    //{
   //     $version = Version::select('*')->first();

   //     return response($version->currentVersion,200);
   // }

    function getWalletUpdate(Request $request)
    {
        $dat = User::where('id',$request->id)->first();
        

        if($dat)
        {
            return response([
                "response_code"=>"200",
                "response_data"=>$dat->bounty,
                "response_msg"=>"wallet details"
                ],200);
        }
    }

    function promoteUser(Request $request)
    {
        $user = User::where('id',$request->id)->first();

        if($user)
        {
            User::where('id',$request->id)->update(["isInfluencer"=>1]);
        }
        header('Location:users');
    }
    function demoteUser(Request $request)
    {
        $user = User::where('id',$request->id)->first();

        if($user)
        {
            User::where('id',$request->id)->update(["isInfluencer"=>0]);
        }
        header('Location:users');
    }

    function Login(Request $request)
    {
        //$uri = "https://www.fast2sms.com/dev/bulkV2";
       $auth = $this->CheckHeader($request);

       if(!$auth)
       {
             return response([
            'response_code'=>'203',
            'response_msg'=>'Auth failed',
            'response_data'=>null
            ],203);
       }

       $data = User::where('phone',$request->data)->first();

       if($data)
       {
          return response([
            'response_code'=>'200',
            'response_msg'=>'valid',
            'response_data'=>$data
            ],200);
       }

       return response([
            'response_code'=>'401',
            'response_msg'=>'Login failed',
            'response_data'=>null
            ],401);
    }

    function SignUp(Request $request)
    {
        $auth = $this->CheckHeader($request);
        $firstLogin = true;

       if(!$auth)
       {
             return response([
            'response_code'=>'203',
            'response_msg'=>'Auth failed',
            'response_data'=>null
            ],203);
       }

       $data = User::where('userName',$request->username)->first();

       if($data)
       {
         return response([
            'response_code'=>'401',
            'response_msg'=>'Username exists already',
            'response_data'=>null
            ],401);
       }

       $data = User::where('email',$request->email)->first();
       if($data)
       {
         return response([
            'response_code'=>'401',
            'response_msg'=>'email exists already',
            'response_data'=>null
            ],401);
       }
       $data = User::where('phone',$request->phone)->first();
       if($data)
       {
         return response([
            'response_code'=>'401',
            'response_msg'=>'phone number exists already',
            'response_data'=>null
            ],401);
       }

       $imageSavedStat = $this->getProfImage($request);

        $profUrl = url('/profilePictures/'.$request->username.'.png?'.rand(10000000,900000000));

        if(!$imageSavedStat)
        {
            $profUrl = url('/profilePictures/default.png');
        }

        
        $done = User::insert([

            "name"=> $request->name,
            "email"=>$request->email,
            "userName"=>$request->username,
            "phone"=>$request->phone,
            "bounty"=>10,
            "profilePhotoPath"=>$profUrl,
            "referal"=>$this->generateRandomString(),
            "refferdBy"=>$request->refferd
        ]);

        if(!empty($request->refferd)){
        $influencer = User::where('referal',$request->refferd)->first();
        if($influencer)
        {
            User::where('id',$influencer->id)->update(["refferd"=>($influencer->refferd + 1),"bounty"=>($influencer->bounty+10)]);
        }
        }
        if($done)
        {
            
            $returnDat = User::where('email',$request->email)->first();
            $this->InitTransaction(10,$returnDat->id);
            return response([
            'response_code'=>'200',
            'response_msg'=>'account is registerd',
            'response_data'=>$returnDat
            ],200);
        }

         return response([
            'response_code'=>'401',
            'response_msg'=>'details are missing',
            'response_data'=>null
            ],401);
    }

    function getProfImage(Request $request)
    {
        $base64Encoded = $request->imageencoded;
        if(!empty($base64Encoded))
            {
                $usrName = $request->username;
              $poc =  \Image::make($base64Encoded)->save(public_path('profilePictures/'.$usrName.'.png'));
              if($poc)
              {
               return true;
               }
               return false;
            }
            
            return false;
    }

    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function moneyOperations(Request $request)
    {
    $check = $this->CheckHeader($request);

    if(!$check)
    {
        return response([
            'response_code'=>'203',
            'response_msg'=>'Auth failed',
            'response_data'=>null
            ],203);
    }
        $data = User::where('id',$request->id)->first();

        if($request->type == "add")
        $nowAmt = $data->bounty + $request->amount;
        else
        $nowAmt = $data->bounty - $request->amount;

        $update = User::where('id',$request->id)->update(['bounty'=>$nowAmt]);

        if($update)
        {
            $this->InitTransaction($request->amount,$request->id);
            return response([
            "response_code"=>"200",
            "response_msg"=> "bounty updated",
            "response_data"=>User::where('id',$request->id)->first()->bounty
            ],200);
        }

        return response([
        "response_code"=>"401",
        "response_msg"=>"bounty not updated",
        "response_data"=>null
        ],401);
    }

    function SetBankDetails(Request $request)
    {
        $user = User::where('id',$request->userId)->first();
        $bank = Bankdetail::where('userId',$request->userId)->first();

        if($user && $bank)
        {
            Bankdetail::where('userId',$request->userId)->update(["holderName"=>$request->holderName,"ifsc"=>$request->ifsc,"accountNumber"=>$request->accountNumber,"bankName"=>$request->bankName]);
        }
        else if($user)
        {
            Bankdetail:: insert(["userId"=>$request->userId,"holderName"=>$request->holderName,"ifsc"=>$request->ifsc,"accountNumber"=>$request->accountNumber,"bankName"=>$request->bankName]);
        }
        else
        {
            return response([
                "response_code"=>"201",
                "response_msg"=>"No data found",
                "response_data"=>null
                ]);
        }
        $bank = Bankdetail::where('userId',$request->userId)->first();

        return response([
                "response_code"=>"200",
                "response_msg"=>"data processed",
                "response_data"=>$bank
                ]);

    }

    function GetBankStatus(Request $request)
    {
        $bank = Bankdetail::where('userId',$request->userId)->first();

        if($bank)
        {
            return response([
                "response_code"=>"200",
                "response_msg"=>"data processed",
                "response_data"=>$bank
                ]);
        }
        else
        {
            return response([
                "response_code"=>"201",
                "response_msg"=>"No data found",
                "response_data"=>null
                ]);
        }
    }

    function GetAllBalanceDetails(Request $request)
    {
        $user = User::where('id',$request->userId)->first();

        if($user)
        {
            return response([
                "response_code"=>"200",
                "response_msg"=>$user->bounty.','.$user->winBounty,
                "response_data"=>null
                ]);
        }

        return response([
                "response_code"=>"201",
                "response_msg"=>"Invalid user",
                "response_data"=>null
                ]);
    }

    function GetWithdrawRequest(Request $request)
    {
        $user = User::where('id',$request->userId)->first();

        if($user)
        {
            if($user->winBounty < $request->amount)
                return response([
                "response_code"=>"201",
                "response_msg"=>"Invalid amount",
                "response_data"=>null
                ]);

            Paymentrequest::insert(["userId"=>$request->userId,"amount"=>$request->amount]);
            User::where('id',$request->userId)->update(["winBounty"=>($user->winBounty-$request->amount)]);
            return response([
                "response_code"=>"200",
                "response_msg"=>"Request sent",
                "response_data"=>$user->winBounty-$request->amount
                ]);
        }
    }

    function updateProfile(Request $request)
    {
        $auth = $this->CheckHeader($request);
        

       if(!$auth)
       {
             return response([
            'response_code'=>'203',
            'response_msg'=>'Auth failed',
            'response_data'=>null
            ],203);
       }

      $result = $this->getProfImage($request);

      if($result)
        $profUrl = url('/profilePictures/'.$request->username.'.png?'.rand(10000000,900000000));

        

      if($result)
        $dat = User::where('id',$request->id)->update(['name'=>$request->name,'email'=>$request->email,'profilePhotoPath'=>$profUrl]);
      else
          $dat = User::where('id',$request->id)->update(['name'=>$request->name,'email'=>$request->email]);

        if($dat)
        {
            $data = User::where('id',$request->id)->first();
            return response([
        "response_code"=>"200",
        "response_msg"=>"Profile is updated",
        "response_data"=>$data
        ],200);
        }
        else
        {
            return response([
        "response_code"=>"401",
        "response_msg"=>"Profile update failed",
        "response_data"=>null
        ],401);
        }
    }

    function changeStat(Request $request)
    {
        $currentTime = date('y-m-d h:i:s');
        $dat = User::where('id',$request->id)->update(['isOnline'=>$request->stat,'lastLogin'=>$currentTime]);

        if($dat)
        {
            return response([
        "response_code"=>"200",
        "response_msg"=>"stat changed",
        "response_data"=>null
        ],200);   
        }
    }

    function uploadDeviceId(Request $request)
    {
        $auth = $this->CheckHeader($request);
        

       if(!$auth)
       {
             return response([
            'response_code'=>'203',
            'response_msg'=>'Auth failed',
            'response_data'=>null
            ],203);
       }

        $dat = User::where('id',$request->id)->update(['deviceId'=>$request->deviceId]);
        if($dat)
        {
            return response([
        "response_code"=>"200",
        "response_msg"=>"dev id updated",
        "response_data"=>null
        ],200);   
        }
    }

    function getTokens()
    {
       $data = Token::select('*')->get();

       if($data)
       {
           return response([
        "response_code"=>"200",
        "response_msg"=>"tokens are fetched",
        "response_data"=>$data
        ],200);
       }
       else
       {
           return response([
        "response_code"=>"401",
        "response_msg"=>"Server response: [null] out of service",
        "response_data"=>null
        ],401);
       }
    }
    function getAvailableSpins()
    {
        $data = Spin::select('*')->get();

       if($data)
       {
           return response([
        "response_code"=>"200",
        "response_msg"=>"tokens are fetched",
        "response_data"=>$data
        ],200);
       }
       else
       {
           return response([
        "response_code"=>"401",
        "response_msg"=>"Server response: [null] out of service",
        "response_data"=>null
        ],401);
       }
    }

    function GetGeneralDetails(Request $request)
    {
        $user = User::where('id',$request->userId)->first();

        $data = array( ["win"=>$user->totalWon,"match"=>$user->totalMatch,"devId"=>$user->deviceId]);

        if($user)
        {
            return response([
        "response_code"=>"200",
        "response_msg"=>"response received",
        "response_data"=>$data
        ],200);
        }
        else
        {
            return response([
        "response_code"=>"201",
        "response_msg"=>"no user",
        "response_data"=>null
        ],200);
        }
    }

    function getMatchDetails(Request $request)
    {
        $existance = Matche::where('roomId',$request->roomCode)->first();
        $user = User::where('id',$request->userId)->first();
        if($existance)
        {
            if($user->bounty >= $existance->amount)
            {
                return response([
        "response_code"=>"200",
        "response_msg"=>"Match allowed",
        "response_data"=>null
        ],200);
            }
            else
            {
                return response([
        "response_code"=>"401",
        "response_msg"=>"Not enough money to join!",
        "response_data"=>null
        ],401);
            }
        }
        return response([
        "response_code"=>"401",
        "response_msg"=>"Joined failed",
        "response_data"=>null
        ],401);
    }

    function joinMatch(Request $request)
    {
        $Balance = User::where('userName',$request->userName)->first();

            if($Balance->bounty < $request->amount)
                return response([
        "response_code"=>"401",
        "response_msg"=>"joined failed",
        "response_data"=>null
        ],401);

        $existance = Matche::where('roomId',$request->roomCode)->first();
        //$data;
        if($existance)
        {
            
            $dat = Matche::where('roomId',$request->roomCode)->update(["players"=>($existance->players.','.$request->userName),"playerCount"=>$existance->playerCount+1,"roomId"=>($existance->roomId.'GGRI')]);
            $balanceCut = User::where('userName',$request->userName)->update(["bounty"=>($Balance->bounty - $request->amount),"totalMatch"=>$Balance->totalMatch+1]);

            if($dat && $balanceCut)
            {
                $data = Matche::where('roomId',$request->roomCode.'GGRI')->first();
                return response([
        "response_code"=>"200",
        "response_msg"=>"joined successfully",
        "response_data"=>$data->matchId
        ],200);
            }
        }
        else
        {
            $matchId = $this->generateRandomString();
            $ins = Matche::insert(["roomId"=>$request->roomCode,"matchId"=>$matchId,"amount"=>$request->amount,"players"=>$request->userName]);
            $balanceCut = User::where('userName',$request->userName)->update(["bounty"=>($Balance->bounty - $request->amount),"totalMatch"=>$Balance->totalMatch+1]);
            if($ins && $balanceCut)
            {
                $data = Matche::where('roomId',$request->roomCode)->first();
                return response([
        "response_code"=>"200",
        "response_msg"=>"match created successfully",
        "response_data"=>$data->matchId
        ],200);
            }
        }

        
    }

    function checkDevId(Request $request)
    {
        $user = User::where('id',$request->userId)->first();

        if($user)
        {
            return response($user->deviceId);
        }
        else
        {
            return response("Invalid");
        }
    }

    function createMatch(Request $request)
    {
        $existance = Matche::where('roomId',$request->roomCode)->first();

        if($existance)
        {
            return response([
        "response_code"=>"201",
        "response_msg"=>"Can't create room with this name.",
        "response_data"=>null
        ],201);
        }

        
            
                return response([
        "response_code"=>"200",
        "response_msg"=>"match created successfully",
        "response_data"=>null
        ],200);
            
    }

    function matchIsCanceled(Request $request)
        {
          
                $existance = Matche::where('matchId',$request->matchId)->first();

                if($existance)
                    {
                       $users = explode(',',$existance->players);

                       $user = $users[0];
                       
                                if($user == $request->userName)
                                    {
                                        $fetchUser = User::where('userName',$user)->first();
                                        if($fetchUser)
                                            {
                                                $returnBalance = User::where('userName',$user)->update(["bounty"=>($fetchUser->bounty+$existance->amount)]);
                                                $deleteMatch = Matche::where('matchId',$request->matchId)->delete();
                                                $usr= User::where('userName',$user)->first();
                                                if($returnBalance && $deleteMatch)
                                                    {
                                                    $this->InitTransaction($existance->amount,$usr->id,"Match cancel refund");
                                                        return response([
                                                            "response_code"=>"200",
                                                            "response_msg"=>"amount refunded",
                                                            "response_data"=>($fetchUser->bounty+$existance->amount)
                                                            ],200);
                                                            
                                                    }
                                            }
                                    }

                                                            return response([
                                                            "response_code"=>"401",
                                                            "response_msg"=>"user does not exists",
                                                            "response_data"=>null
                                                            ],401);
                           
                    }

                                                            return response([
                                                            "response_code"=>"401",
                                                            "response_msg"=>"match does not exists",
                                                            "response_data"=>null
                                                            ],401);
        }

    function showWin(Request $request)
    {
        $result = Result::where('matchId',$request->matchId)->first();

        if($result)
        {
            return response([
                             "response_code"=>"401",
                             "response_msg"=>"Result is already there",
                             "response_data"=>null
                           ],401);
        }
        else
        {
            $user  = User::where('userName',$request->userName)->first();
            $match = Matche::where('matchId',$request->matchId)->first();

            if($user && $match)
            {
                $influencerAmount = (($match->amount * 2) * 20) / 100;
                $decidedAmt = (($match->amount * 2) - $influencerAmount);
                $influencerAmount = $influencerAmount / 2;

                $i = User::where('userName',$request->userName)->first();
                $influencer = User::where('referal',$i->refferdBy)->first();

                if($influencer){
                User::where('userName',$influencer->userName)->update(['winBounty'=> ($influencer->winBounty+$influencerAmount)]);
                }
                

                $u= User::where('userName',$request->userName)->update(['winBounty'=> ($user->winBounty+$decidedAmt),'totalWon'=>$user->totalWon+1]);
               $r = Result::insert(['roomCode'=>$match->roomId,'matchId'=>$match->matchId,'winner'=>$request->userName,'amount'=>$match->amount]);

               if($u && $r)
               {
                   $this->InitTransaction(($decidedAmt),$user->id,"Match win");

                   if($influencer)
                   $this->InitTransaction(($influencerAmount),$influencer->id,"Referal amount");

                   return response([
                             "response_code"=>"200",
                             "response_msg"=>"updated successfully",
                             "response_data"=>null
                           ],200);
               }
               else
               {
                   return response([
                             "response_code"=>"401",
                             "response_msg"=>"cant update",
                             "response_data"=>null
                           ],401);
               }
            }
            else
            {
                return response([
                             "response_code"=>"401",
                             "response_msg"=>"No credentials",
                             "response_data"=>null
                           ],401);
            }
        }
    }

    function addBenificiary(Request $request)
    {
        $ben = Vpa::where('userId',$request->userId)->first();
        if($ben)
        {
            return response([
                             "response_code"=>"202",
                             "response_msg"=>"Already added",
                             "response_data"=>$ben->upi
                           ],401);
        }
        $benId = $this->generateRandomString();
        $user = User::where('id',$request->userId)->first();
        $ins = Vpa::insert(["userId"=>$user->id,"upi"=>$request->upi,"benId"=>$benId,"name"=>$user->name,"email"=>$user->email,"phone"=>$user->phone,"address"=>$request->address]);

        if($ins)
        {
            $dat = Vpa::where('userId',$request->userId)->first();
            return response([
                             "response_code"=>"200",
                             "response_msg"=>"data inserted",
                             "response_data"=>$dat->upi
                           ],401);
        }
    }
    
}
